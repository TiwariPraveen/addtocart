import React,{useContext} from 'react';
import {View,Text} from 'react-native'
import {CartContext} from './CartContext';

export const Cart = () => {
  const [cart, setCart] = useContext(CartContext);
  const totalPrice = cart.reduce((acc, curr) => acc + curr.price, 0);

  return (
    <View>
      <Text  style={{ fontSize: 18, fontWeight: 'bold' }}>items in cart : {cart.length}</Text>
      <Text  style={{ fontSize: 18, fontWeight: 'bold',marginTop:10 }}>total price : {totalPrice}</Text>
    </View>
  )
}
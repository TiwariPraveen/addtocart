import React, { useContext } from 'react';
import { View, Text, Button } from 'react-native'
import { CartContext } from './CartContext';

export const Tshirt = (props) => {
    const [cart, setCart] = useContext(CartContext);

    const addToCart = () => {
        const tshirt = { name: props.name, price: props.price };
        setCart(currentState => [...currentState, tshirt]);
    }
    return (
        <View>
            <View style={{ borderBottomWidth: 1,marginTop:30 }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{props.name}</Text>
                <Text style={{ fontSize: 24, fontWeight: 'bold' }}>{props.price}</Text>
                <Button title="Add to cart" onPress={addToCart} />
            </View>
        </View>
    )
}
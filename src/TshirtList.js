import React from 'react';
import {View,Text} from 'react-native'
import { Tshirt } from './Tshirt';

export const TshirtList = () => {

  const database = [
    { name: "red tshirt", price: 100, id: 1 },
    { name: "yellow tshirt", price: 124, id: 2 },
    { name: "blue tshirt", price: 180, id: 3 },
  ]

  return (
    <View>
      {
        database.map(item => (
          <Tshirt name={item.name} price={item.price} key={item.id} />
        ))
      }
    </View>
  )
}
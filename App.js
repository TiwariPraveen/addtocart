import React from 'react'
import { View, Text } from 'react-native'
import {CartProvider} from './src/CartContext'
import {TshirtList} from './src/TshirtList'
import {Cart} from './src/Cart'
export default function App() {
  return (
    <CartProvider>
    <View>
      <Cart/>
     <TshirtList/>
    </View>
    </CartProvider>
  )
}


// import React from 'react';
// import { View, Text } from 'react-native'
// import Theme from './src2/theme';
// import ThemedButton from './src2/ThemedButton';

// class App extends React.Component {
//   state = {  
//     theme: 'dark',  
//     themes: ['light', 'dark']  
//   };

//   handleSelect = (evt) => {
//     console.log('Changing value to ' + evt.target.value);

//     this.setState({  
//       theme: evt.target.value  
//     });  
//   };

//   render() {
//     return (
//      <View>
//       <h2>Any component</h2>
//       <select value = {this.state.theme} 
//        onChange ={this.handleSelect}> 
//       { this.state.themes.map(t => 
//       <option value = {t} >{t}</option>)
//       }
//       </select>
//       <div>
//       Selected theme: {this.state.theme} 
//       </div>
//       <Theme.Provider value ={this.state.theme}>
//         <ThemedButton theme={this.state.theme} />
//       </Theme.Provider>
//       </View>
//     );

//   }
// }

// export default App;
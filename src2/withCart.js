import CartContext from './cart';
import React from 'react';

export const withCart = (Component) => {
 return function fn(props) {
   return (
     <CartContext.Consumer>
     {(context) => <Component {...props} {...context} />}
     </CartContext.Consumer>
   );
 };
};
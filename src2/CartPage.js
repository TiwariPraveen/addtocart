import React from 'react';
import {View,Text,Button,TouchableOpacity} from 'react-native'
import CartContext from './cart';

const products = [{
 id: 1,
 title: 'Fortnite'
}, {
 id: 2,
 title: 'Doom'
}, {
 id: 3,
 title: 'Quake'
}]

const CartPage = () => (
 <CartContext.Consumer>
 {({ cart, addItem }) => (
 <View>
   <View>
   <Text>Product list</Text>
   {products.map(p => 
   <TouchableOpacity onPress={() => addItem(p)}>
   <Text>{p.title} . </Text>
   </TouchableOpacity>
   )}
   </View>
   <View>
     <Text>Cart</Text>
     {cart.map(item => <div> {item.title} </div>)}
   </View>
 </View>
 )}
 </CartContext.Consumer>
);

export default CartPage;
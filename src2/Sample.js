import React from 'react';
import {View,Text} from 'react-native'
import Theme from './theme';
import ThemedButton from './ThemedButton';
const Sample = () => (
    <Theme.Provider value='dark'>
    <ThemedButton />
  </Theme.Provider>
  );

export default Sample;
import React from 'react';

const CartContext = React.createContext({
 cart: void 0,
 addItem: () => {}

});

export default CartContext;
import React from 'react';
import {View,Text} from 'react-native'
import withCart from './withCart';

class Header extends React.Component {
  render() {
    const { cart } = this.props;
    
    return (
        <View>
      {
      cart.length === ?
      
      <Text>Empty cart</Text>
       :
      <Text>Items in cart: ({cart.length})</Text>
      }
      </View>
    );
  }

}

const HeaderWithCart = withCart(Header);
export default HeaderWithCart;